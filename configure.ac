m4_define([newport_major], [0])
m4_define([newport_minor], [2020])
m4_define([newport_micro], [1])

m4_define([newport_version], [newport_major.newport_minor.newport_micro])
m4_define([newport_lt_version], [0:5:0])

AC_INIT([newport], newport_version)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_MAINTAINER_MODE([enable])

AC_SUBST([NEWPORT_LT_VERSION],newport_lt_version)
AC_SUBST(NEWPORT_VERSION,newport_version)

# Incremented if the API has incompatible changes
AC_SUBST([NEWPORT_API_VERSION],newport_major)

AM_INIT_AUTOMAKE([-Wno-portability tar-ustar])
AM_SILENT_RULES([yes])

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_STDC_HEADERS
AC_SEARCH_LIBS([strerror],[cposix])
AC_CHECK_FUNCS([memset])
AC_CHECK_HEADERS([fcntl.h stdlib.h string.h])

LT_INIT

AC_ARG_VAR(GDBUS_CODEGEN, [the gdbus-codegen programme ])
AC_PATH_PROG(GDBUS_CODEGEN, gdbus-codegen)
if test -z "$GDBUS_CODEGEN" ; then
  AC_MSG_ERROR([gdbus-codegen not found])
fi

AC_ARG_ENABLE([optimization],
AC_HELP_STRING([--enable-optimization], [enable code optimization through compiler]),[enable_optimization=$enableval],[enable_optimization=no])

if test "x$enable_optimization" = "xyes"; then
	CFLAGS="$CFLAGS -O0 -U_FORTIFY_SOURCE"
fi

#Check if Gobject-introspection enabled
AC_ARG_ENABLE([introspection],
AS_HELP_STRING([--enable-introspection], [build with gobject introspection [default=yes]]),[],[enable_introspection=yes])
AM_CONDITIONAL(HAVE_INTROSPECTION, test "x$enable_introspection" = "xyes")

dnl for gobject-introspection
GOBJECT_INTROSPECTION_CHECK([1.31.1])

NEWPORT_PACKAGES_PUBLIC_GLIB="glib-2.0 gio-2.0"
NEWPORT_PACKAGES_PRIVATE_GLIB="gthread-2.0 gmodule-2.0 gio-unix-2.0"

NEWPORT_PACKAGES_PUBLIC_CURL=""
NEWPORT_PACKAGES_PRIVATE_CURL="libcurl"

NEWPORT_PACKAGES_PUBLIC="$NEWPORT_PACKAGES_PUBLIC_GLIB $NEWPORT_PACKAGES_PUBLIC_CURL "
NEWPORT_PACKAGES_PRIVATE="$NEWPORT_PACKAGES_PRIVATE_GLIB  $NEWPORT_PACKAGES_PRIVATE_CURL "

AC_SUBST([NEWPORT_PACKAGES_PUBLIC])
AC_SUBST([NEWPORT_PACKAGES_PRIVATE])

PKG_CHECK_MODULES([GLIB], [$NEWPORT_PACKAGES_PUBLIC_GLIB $NEWPORT_PACKAGES_PRIVATE_GLIB])
PKG_CHECK_MODULES([CURL], [$NEWPORT_PACKAGES_PUBLIC_CURL $NEWPORT_PACKAGES_PRIVATE_CURL])
PKG_CHECK_MODULES([SOUP], [libsoup-2.4])

if test "x$GCC" = "xyes"; then
 GCC_FLAGS="-g -Wall -Wno-unused-but-set-variable -Wl,--no-as-needed"
fi

AC_SUBST(GCC_FLAGS)

GLIB_GSETTINGS

NEWPORT_FI='-lnewportiface'
AC_SUBST(NEWPORT_FI)

AC_ARG_WITH([systemdunitdir], AC_HELP_STRING([--with-systemdunitdir=DIR],
        [path to systemd service directory]), [path_systemdunit=${withval}],
                [path_systemdunit="`$PKG_CONFIG --variable=systemduserunitdir systemd`"])
if (test -n "${path_systemdunit}"); then
        SYSTEMD_UNITDIR="${path_systemdunit}"
        AC_SUBST(SYSTEMD_UNITDIR)
fi
AM_CONDITIONAL(SYSTEMD, test -n "${path_systemdunit}")
AC_REQUIRE_AUX_FILE([tap-driver.sh])
# installed-tests
AC_ARG_ENABLE([always_build_tests],
              [AS_HELP_STRING([--enable-always-build-tests],
                             [Enable always building tests (default: yes)])],
                             [],
              [enable_always_build_tests=yes])
AC_ARG_ENABLE([installed_tests],
              [AS_HELP_STRING([--enable-installed-tests],
                             [Install test programs (default: no)])],
                             [],
              [enable_installed_tests=no])

AM_CONDITIONAL([ENABLE_ALWAYS_BUILD_TESTS],
               [test "$enable_always_build_tests" = "yes"])
AC_SUBST([ENABLE_ALWAYS_BUILD_TESTS],[$enable_always_build_tests])

AM_CONDITIONAL([ENABLE_INSTALLED_TESTS],
               [test "$enable_installed_tests" = "yes"])
AC_SUBST([ENABLE_INSTALLED_TESTS],[$enable_installed_tests])

AX_CODE_COVERAGE
# TODO: fix the warnings and add nano-version support, then change the
# 3rd argument ("is release") to "yes" for non-release versions
AX_COMPILER_FLAGS([], [], [no])

AC_ARG_VAR([GLIB_MKENUMS], [the glib-mkenums tool])
AC_PATH_PROG([GLIB_MKENUMS], [glib-mkenums])
AS_IF([test -z "$GLIB_MKENUMS"],
  [AC_MSG_ERROR([glib-mkenums not found])])

AC_CONFIG_FILES([
Makefile
newport.pc
interface/Makefile
src/Makefile
tests/Makefile
scripts/Makefile
])

AC_OUTPUT

dnl Summary
echo "
                     newport
                      --------------
         documentation                  : ${enable_documentation}
         code coverage                  : ${enable_code_coverage}
         compiler warings               : ${enable_compile_warnings}
         Test suite                     : ${enable_modular_tests}
         Install tests                  : ${enable_installed_tests}
         Compiler Optimization          : ${enable_optimization}
         Introspection                  : ${found_introspection}
"
