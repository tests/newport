/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include "newport-internal.h"


static gboolean move_file_to_actual_location(DownloadUrlInfo *pUrlInfo,GError **error);
static void download_queued_urls(void);

static void _newport_download_finished(GObject *source_object,
		GAsyncResult *result, gpointer user_data)
{
	DownloadUrlInfo *pUrlInfo = (DownloadUrlInfo *) user_data;
	NewportDownload *download = (NewportDownload *) source_object;
	GError *error = NULL;
	gboolean is_download_success;

	newport_download_set_total_size(download,pUrlInfo->progress.u64TotalSize);
	newport_download_set_downloaded_size(download,pUrlInfo->progress.u64CurrentSize);
	newport_download_set_state(download, pUrlInfo->u32CurrentDownloadState);
	NEWPORT_DEBUG("_newport_download_finished %d", pUrlInfo->u32CurrentDownloadState);
    if (!store_url_data_in_db (pUrlInfo, &error))
       {
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      newport_download_set_state (download, pUrlInfo->u32CurrentDownloadState);
      newport_download_set_error (download,
                                  NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED);
         g_clear_pointer(&error,g_error_free);
         return;
       }
  is_download_success = g_task_propagate_boolean (G_TASK (result), &error);
	if (is_download_success)
	{
	    GError *err=NULL;
		gboolean status =move_file_to_actual_location(pUrlInfo,&err);
		if(!status)
		{
			pUrlInfo->u32CurrentDownloadState=NEWPORT_DOWNLOAD_STATE_FAILED;
			newport_download_set_state(download, pUrlInfo->u32CurrentDownloadState);
		    if (!store_url_data_in_db (pUrlInfo, &error))
		       {
		         NEWPORT_WARNING("%s", error->message);
		         g_clear_pointer(&error,g_error_free);
		       }
			unlink_file(pUrlInfo->pTempDownloadPath);
			newport_download_set_error(download,
					NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED);
			NEWPORT_WARNING("%s", err->message);
			g_error_free(err);
            return;
		}
		download_queued_urls();
	}
	else
	{
		if (error)
		{
			NEWPORT_WARNING("ERROR is %s", error->message);
			g_error_free(error);
			return;
		}
		if (pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_CANCELLED)
		{
          if (!dnl_mgr_remove_from_pdi (pUrlInfo, &error))
            {
              NEWPORT_WARNING("%s", error->message);
              g_clear_pointer (&error, g_error_free);
              return;
            }
          g_dbus_object_manager_server_unexport(pDnlMgrObj.object_manager,
                    pUrlInfo->object_path);
			unlink_file(pUrlInfo->pTempDownloadPath);
			pDnlMgrObj.pClientDownloadList = g_list_remove(
					pDnlMgrObj.pClientDownloadList, pUrlInfo);
            newport_download_complete_cancel (download, pUrlInfo->cancel_invocation);
            g_object_unref(pUrlInfo->cancel_invocation);
			download_url_info_free(pUrlInfo);
		}
      else if (pUrlInfo->u32CurrentDownloadState
          == NEWPORT_DOWNLOAD_STATE_PAUSED_BY_USER)
        {
          newport_download_complete_pause (download, pUrlInfo->pause_invocation);
          g_object_unref (pUrlInfo->pause_invocation);
        }
	}

}

void start_new_url_download(NewportDownload *Download,
		DownloadUrlInfo *pUrlInfo)
{
	GTask *task = NULL;
	task = g_task_new(Download, NULL,
	/* This is called when the task finishes */
	(GAsyncReadyCallback) _newport_download_finished, pUrlInfo);
	g_task_set_task_data(task, pUrlInfo, NULL);
	newport_download_set_state(Download, pUrlInfo->u32CurrentDownloadState);
	g_task_run_in_thread(task, (GTaskThreadFunc) start_new_download_thread);
	g_object_unref(task);
}

void initialize_url_info(DownloadUrlInfo *pUrlInfo)
{
	pUrlInfo->pAppName = NULL;
	pUrlInfo->object_path = NULL;
	pUrlInfo->pDownloadPath = NULL;
	pUrlInfo->pUrl = NULL;
	pUrlInfo->uin64ElapsedTimeBeg = 0;
	pUrlInfo->FileOffset = 0;
	pUrlInfo->OldFileOffset = 0;
	pUrlInfo->FileOffsetBeginning = 0;
	pUrlInfo->uin8RetryCount = 0;
	pUrlInfo->lCurlTime = 0;
	pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_QUEUED;
	pUrlInfo->pCurlObg = NULL;
	pUrlInfo->pDownloadFileName = NULL;
	pUrlInfo->pause_invocation=NULL;
	pUrlInfo->cancel_invocation=NULL;
	pUrlInfo->progress.u8ProgressCount = 0;
	pUrlInfo->progress.u64TotalSize = 0;
	pUrlInfo->progress.u64CurrentSize = 0;
	pUrlInfo->progress.u64DownloadSpeed = 0;
	pUrlInfo->progress.u64ElapsedTime = 0;
	pUrlInfo->progress.u64RemainingTime = 0;

}

void
resume_url_download (NewportDownload *Download, DownloadUrlInfo *pUrlInfo,
                     GError **error)
{
	NEWPORT_DEBUG("resume_url_download ");
  g_return_if_fail (pUrlInfo != NULL);
  if (pDnlMgrObj.uinDownloadCount >= g_settings_get_uint (
      pDnlMgrObj.schema, "max-parallel-download"))
    {
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_QUEUED;

      if (!store_url_data_in_db (pUrlInfo, error))
        {
          pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
          newport_download_set_error (Download,
                                      NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED);
        }
      newport_download_set_state (Download, pUrlInfo->u32CurrentDownloadState);
    }
  else
    {
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_IN_PROGRESS;
      if (!store_url_data_in_db (pUrlInfo, error))
        {
          pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
          newport_download_set_state (Download,
                                      pUrlInfo->u32CurrentDownloadState);
          newport_download_set_error (Download,
                                      NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED);
          return;
        }
      pDnlMgrObj.uinDownloadCount++;
      newport_download_set_state (Download, pUrlInfo->u32CurrentDownloadState);
      start_new_url_download (Download, pUrlInfo);
    }
}

DownloadUrlInfo* check_for_valid_url(const gchar *pUrl, const gchar *pAppName)
{

  DownloadUrlInfo *pUrlInfo = look_for_url_in_db (pUrl, pAppName);
  if (pUrlInfo == NULL)
    {
      NEWPORT_DEBUG ("pUrlInfo is null");
    }
  return pUrlInfo;
}

DownloadUrlInfo* look_for_url_in_db(const gchar* pUrl, const gchar* pAppName)
{
  GList *iter;
  for (iter = pDnlMgrObj.pClientDownloadList; iter!=NULL;
      iter = g_list_next (iter))
    {
      DownloadUrlInfo *pUrlInfo = iter->data;

      if ((!g_strcmp0 (pUrlInfo->pAppName, pAppName))
                      && (!g_strcmp0 (pUrlInfo->pUrl, pUrl)))
        {
          NEWPORT_DEBUG(" Valid URL found in db");
          return pUrlInfo;
        }
    }
  return NULL;
}

void download_pending_urls(void)
{
  GList *iter;

  NEWPORT_DEBUG (" Downloading Pending URL");
  for (iter = pDnlMgrObj.pClientDownloadList; iter!=NULL;
      iter = g_list_next (iter))
    {
      DownloadUrlInfo *pUrlInfo = iter->data;

      if ((pUrlInfo->u32CurrentDownloadState
          == NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM))
        {
          GDBusObject *dbus_object =
              newport_download_url_info_ensure_dbus_object (pUrlInfo);

          NewportDownload *Download =
              (NewportDownload *) g_dbus_object_get_interface (
                  dbus_object, "org.apertis.Newport.Download");
          /* This doesn't require error handling as it is handled for each download by changing their states to NEWPORT_DOWNLOAD_STATE_FAILED */
          resume_url_download (Download, pUrlInfo, NULL);
          g_clear_object(&dbus_object);
        }
    }
}

static void download_queued_urls(void)
{
  GList *iter;

  NEWPORT_DEBUG ("Download queued URLs");
  for (iter = pDnlMgrObj.pClientDownloadList; iter != NULL;
      iter = g_list_next (iter))
    {
      DownloadUrlInfo *pUrlInfo = iter->data;

      if ((pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_QUEUED))
        {
          GDBusObject *dbus_object =
              newport_download_url_info_ensure_dbus_object (pUrlInfo);
          NewportDownload *Download =
              (NewportDownload *) g_dbus_object_get_interface (
                  dbus_object, "org.apertis.Newport.Download");
          /* This doesn't require error handling as it is handled for each download by changing their states to NEWPORT_DOWNLOAD_STATE_FAILED */
          resume_url_download (Download, pUrlInfo, NULL);
          g_clear_object(&dbus_object);
        }
    }
}


void halt_all_url_dnl(void)
{
  GList *iter;
  for (iter = pDnlMgrObj.pClientDownloadList; iter != NULL;
      iter = g_list_next (iter))
    {
      DownloadUrlInfo *pUrlInfo = iter->data;
      NEWPORT_DEBUG("Halt the downloading URL %s", pUrlInfo->pUrl);
      if (pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_IN_PROGRESS)
        {
          pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM;
        }
    }
}


gboolean move_file_to_actual_location(DownloadUrlInfo *pUrlInfo,GError **error)
{
    GFile *source=g_file_new_for_path (pUrlInfo->pTempDownloadPath);
    GFile *destination=g_file_new_for_path (pUrlInfo->pDownloadPath);
    gboolean status = g_file_move (source,
    destination,
    G_FILE_COPY_OVERWRITE,
    NULL,
    NULL,
    NULL,
    error);
    g_clear_object(&source);
    g_clear_object(&destination);
    return status;

}

gchar *newport_build_temporary_download_path(const gchar *file_name)
{
  gchar *temp_download_path = NULL;

  g_return_val_if_fail (strchr (file_name, '/') == NULL, NULL);

  temp_download_path =
    g_build_filename (g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
                      "Storage", file_name, NULL);

  return temp_download_path;
}



static void
newport_connman_get_services_clb (GObject *source_object, GAsyncResult *res,
                                  gpointer user_data)
{
  GError *error = NULL;
  gchar **prefered_networks = NULL;
  GVariant *return_service_list = NULL;

  return_service_list =
    g_dbus_connection_call_finish (pDnlMgrObj.system_dbus_connection, res,
                                   &error);
  if (error)
    {
      g_error_free (error);
      pDnlMgrObj.is_preferred_network = FALSE;
      halt_all_url_dnl ();
      return;
    }
  prefered_networks = g_settings_get_strv (pDnlMgrObj.schema,
                                           "preferred-networks");
  if (return_service_list != NULL)
    {
      const gchar *field = NULL;
      GVariantIter iter, array_iter;
      GVariant *array = NULL, *array_value = NULL;
      GVariantIter *fields;
      GVariant *value1 = NULL;
      gboolean is_preferred_network = FALSE, is_online = FALSE;
      g_variant_iter_init (&iter, return_service_list);
      array = g_variant_iter_next_value (&iter);
      g_return_if_fail (array != NULL);
      g_variant_iter_init (&array_iter, array);
      while ((array_value = g_variant_iter_next_value (&array_iter)) != NULL)
        {
          g_variant_get (array_value, "(oa{sv})", NULL, &fields);
          while (g_variant_iter_next (fields, "{&sv}", &field, &value1))
            {
              if (!g_strcmp0 (field, "Type"))
                {
                  if (g_strv_contains (
                      (const gchar * const *) prefered_networks,
                      g_variant_get_string (value1, NULL)))
                    {
                      is_preferred_network = TRUE;
                    }
                }
              if (!g_strcmp0 (field, "State"))
                {
                  if ((g_strcmp0 (g_variant_get_string (value1, NULL), "ready")
                      == 0)
                      || (g_strcmp0 (g_variant_get_string (value1, NULL),
                                     "online") == 0))
                    is_online = TRUE;

                }
              g_clear_pointer (&value1, (GDestroyNotify) g_variant_unref);
            }
          g_clear_pointer (&array_value, (GDestroyNotify) g_variant_unref);
          if (fields)
            g_variant_iter_free (fields);

          if (is_preferred_network && is_online)
            {
              break;
            }
        }

      pDnlMgrObj.is_preferred_network = is_preferred_network && is_online;
      if (pDnlMgrObj.is_preferred_network)
        download_pending_urls ();
      else
        halt_all_url_dnl ();
      g_strfreev (prefered_networks);
      g_clear_pointer (&array, (GDestroyNotify) g_variant_unref);
      g_clear_pointer (&return_service_list, (GDestroyNotify) g_variant_unref);
    }

}

void
newport_check_for_preferred_network (void)
{
  g_dbus_connection_call (pDnlMgrObj.system_dbus_connection, "net.connman", "/",
                          "net.connman.Manager", "GetServices", NULL, NULL,
                          G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                          newport_connman_get_services_clb, NULL);
}


DownloadUrlInfo *
download_url_info_copy (DownloadUrlInfo *url_info)
{
  DownloadUrlInfo *copy_url_info = g_new0 (DownloadUrlInfo, 1);
  initialize_url_info (copy_url_info);
  copy_url_info->pAppName = g_strdup (url_info->pAppName);
  copy_url_info->pUrl = g_strdup (url_info->pUrl);
  copy_url_info->pDownloadPath = g_strdup (url_info->pDownloadPath);
  copy_url_info->object_path = g_strdup (url_info->object_path);
  copy_url_info->pTempDownloadPath = g_strdup (url_info->pTempDownloadPath);
  copy_url_info->pDownloadFileName = g_strdup (url_info->pDownloadFileName);
  copy_url_info->u32CurrentDownloadState = url_info->u32CurrentDownloadState;
  copy_url_info->progress.pProgressPercent =
      url_info->progress.pProgressPercent;
  copy_url_info->progress.u8ProgressCount = url_info->progress.u8ProgressCount;
  copy_url_info->progress.u64TotalSize = url_info->progress.u64TotalSize;
  copy_url_info->progress.u64CurrentSize = url_info->progress.u64CurrentSize;
  copy_url_info->progress.u64DownloadSpeed =
      url_info->progress.u64DownloadSpeed;
  copy_url_info->progress.u64ElapsedTime = url_info->progress.u64ElapsedTime;
  copy_url_info->progress.u64RemainingTime =
      url_info->progress.u64RemainingTime;
  copy_url_info->pCurlObg = g_object_ref(url_info->pCurlObg);
  g_stpcpy(copy_url_info->pCurlErrorString,url_info->pCurlErrorString);
  copy_url_info->FileOffset = url_info->FileOffset;
  copy_url_info->OldFileOffset = url_info->OldFileOffset;
  copy_url_info->FileOffsetBeginning = url_info->FileOffsetBeginning;
  copy_url_info->uin8RetryCount = url_info->uin8RetryCount;
  copy_url_info->lCurlTime = url_info->uin8RetryCount;
  copy_url_info->uin64ElapsedTimeBeg = url_info->uin64ElapsedTimeBeg;
  return copy_url_info;
}

/* Returns: (transfer full): newly allocated string,has to be freed with g_free() */
gchar*
newport_download_get_group_name (DownloadUrlInfo *url_info)
{
  GString *out = NULL;

  g_return_val_if_fail (url_info != NULL, NULL);
  g_return_val_if_fail (url_info->pAppName != NULL, NULL);
  g_return_val_if_fail (url_info->pUrl != NULL, NULL);
  g_return_val_if_fail (url_info->pDownloadPath != NULL, NULL);

  out = g_string_new ("");
  g_string_append_uri_escaped (out, url_info->pAppName, NULL, TRUE);
  g_string_append_c (out, ':');
  g_string_append_uri_escaped (out, url_info->pUrl, NULL, TRUE);
  g_string_append_c (out, ':');
  g_string_append_uri_escaped (out, url_info->pDownloadPath, NULL, TRUE);

  if (out->len > 255)
    g_string_set_size (out, 255);
  return g_string_free (out, FALSE);
}
