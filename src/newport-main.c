/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib-object.h>
#include "newport-internal.h"
#include <string.h>

/* TODO This pDnlMgrObj need to be fixed */
DownloadMgr pDnlMgrObj={0};

static void dnl_mgr_register_exit_functionalities(void);
static void dnl_mgr_initialize_all_resources(GDBusConnection *pConnection);

static void
on_bus_acquired(GDBusConnection *pConnection, const gchar *pName,
		gpointer pUserData);
static void
on_name_acquired(GDBusConnection *pConnection, const gchar *pName,
		gpointer pUserData);

static void
on_name_lost(GDBusConnection *pConnection, const gchar *pName,
		gpointer pUserData);

static void
on_bus_acquired (GDBusConnection *pConnection, const gchar *pName,
                 gpointer pUserData)
{

  NEWPORT_DEBUG("%s Bus acquired ", pName);
  dnl_mgr_initialize_all_resources (pConnection);
  pDnlMgrObj.pDnlMgrDbusObj = newport_service_skeleton_new ();
  g_signal_connect (pDnlMgrObj.pDnlMgrDbusObj, "handle-start-download",
                    G_CALLBACK (handle_start_download), pUserData);
  g_signal_connect (pDnlMgrObj.pDnlMgrDbusObj, "handle-get-downloads",
                    G_CALLBACK (handle_get_downloads), pUserData);
  g_signal_connect (pDnlMgrObj.pDnlMgrDbusObj, "handle-get-download-for-url",
                    G_CALLBACK (handle_get_download_for_url), pUserData);

	if (!g_dbus_interface_skeleton_export(
			G_DBUS_INTERFACE_SKELETON(pDnlMgrObj.pDnlMgrDbusObj), pConnection,
			"/org/apertis/Newport/Service", NULL))
	{
		NEWPORT_CRITICAL("Interface skeleton export error");
	}

}

static void
on_name_acquired (GDBusConnection *pConnection, const gchar *pName,
                  gpointer pUserData)
{
NEWPORT_DEBUG("%s Name acquired",pName);
}

static void
on_name_lost (GDBusConnection *pConnection, const gchar *pName,
              gpointer pUserData)
{
NEWPORT_CRITICAL (" %s name lost", pName);
}

gint main(gint argc, gchar *argv[])
{

	GMainLoop *pMainLoop = NULL;

	guint owner_id;

	owner_id = g_bus_own_name(G_BUS_TYPE_SESSION,           // bus type
			"org.apertis.Newport",       // interface pName
			G_BUS_NAME_OWNER_FLAGS_NONE, // bus own flag, can be used to take away the bus and give it to another service
			on_bus_acquired,        // callback invoked when the bus is acquired
			on_name_acquired, // callback invoked when interface pName is acquired
			on_name_lost, // callback invoked when pName is lost to another service or other reason
			NULL,                         // user data
			NULL);                        // user data free func

	pMainLoop = g_main_loop_new(NULL, FALSE);

	g_main_loop_run(pMainLoop);
	g_bus_unown_name(owner_id);
	exit(0);
}

static void
_create_default_download_objects_from_db (void)
{

	GList *list = NULL;
	for (list = pDnlMgrObj.pClientDownloadList; list != NULL; list = list->next)
	{
		DownloadUrlInfo *url_info = (DownloadUrlInfo *) list->data;
		NewportDownload *download = newport_download_skeleton_new();
		GDBusObjectSkeleton *object = NULL;

		g_signal_connect(download, "handle-pause",
				G_CALLBACK(handle_pause_download), NULL);
		g_signal_connect(download, "handle-cancel",
				G_CALLBACK(handle_cancel_download), NULL);
		g_signal_connect(download, "handle-resume",
				G_CALLBACK(handle_resume_download), NULL);

		newport_download_set_downloading_app(download, url_info->pAppName);
		newport_download_set_url(download, url_info->pUrl);
		newport_download_set_state(download, url_info->u32CurrentDownloadState);
		newport_download_set_error(download,-1);
		newport_download_set_path(download,url_info->pDownloadPath);
		newport_download_set_downloaded_size(download,
				url_info->progress.u64CurrentSize);
		newport_download_set_total_size(download,
				url_info->progress.u64TotalSize);

      object = g_dbus_object_skeleton_new (url_info->object_path);
		g_dbus_object_skeleton_add_interface(object,
				(GDBusInterfaceSkeleton *) download);
		g_dbus_object_manager_server_export_uniquely(pDnlMgrObj.object_manager,
				object);
		  g_object_unref(download);
		  g_object_unref(object);
	}
}



static void
_newport_connman_property_changed_clb (GDBusConnection *connection,
                                       const gchar *sender_name,
                                       const gchar *object_path,
                                       const gchar *interface_name,
                                       const gchar *signal_name,
                                       GVariant *parameters, gpointer user_data)
{
const gchar *name = NULL;
GVariant *params = NULL;

NEWPORT_DEBUG ("newport_connman_property_changed_clb");
g_variant_get (parameters, "(&sv)", &name, &params);
if ((g_strcmp0 (name, "State") == 0))
  {
    if ((g_strcmp0 (g_variant_get_string (params, NULL), "ready") == 0)
        || (g_strcmp0 (g_variant_get_string (params, NULL), "online") == 0))
      {
        newport_check_for_preferred_network ();
      }
    else
      halt_all_url_dnl ();
  }
else if ((g_strcmp0 (name, "Type") == 0))
  {
    newport_check_for_preferred_network ();
  }
  else if ((g_strcmp0 (name, "Proxy") == 0))
    {
      newport_check_for_preferred_network ();
    }

if (params)
  g_variant_unref (params);

}

static void
newport_network_preferences_changed_cb (GSettings *settings, const gchar *key,
                                        gpointer user_data)
{
  if (!g_strcmp0 (key, "preferred-networks"))
    {
      newport_check_for_preferred_network ();
    }
}

static void
dnl_mgr_initialize_all_resources (GDBusConnection *pConnection)
{
  gboolean result;
  GError *error = NULL;
  gchar *pTempDnlDir = NULL;

pDnlMgrObj.system_dbus_connection = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL,
                                                  NULL);
pDnlMgrObj.is_preferred_network=FALSE;
pDnlMgrObj.pClientDownloadList = NULL;
pDnlMgrObj.uinDownloadCount = 0;
pDnlMgrObj.pDnlMgrDbusObj = NULL;
pDnlMgrObj.configuration_schema=g_settings_new ("org.apertis.Newport.Config");
pDnlMgrObj.schema = g_settings_new ("org.apertis.Newport.Preferences");
g_signal_connect (pDnlMgrObj.schema, "changed",
                  G_CALLBACK (newport_network_preferences_changed_cb), NULL);

	//temporary download directory where the download actually happens. This dir will get changed to /Applications folder once created in the target/SDK image
  pTempDnlDir =
    g_build_filename (g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
                      "Storage", NULL);
	/*Create the directory */
	g_mkdir_with_parents(pTempDnlDir, 0755);
	g_free(pTempDnlDir);

	  pDnlMgrObj.object_manager = g_dbus_object_manager_server_new (
	      "/org/apertis/Newport/Download");
	  g_dbus_object_manager_server_set_connection (pDnlMgrObj.object_manager,
	                                               pConnection);
      // retrieve the contents from the db, create a local database and start downloading if any pending downloads
  initialize_database (&error);
  if (error)
    {
      NEWPORT_CRITICAL("%s", error->message);
      g_error_free (error);
      exit (1);
    }
  result = retrieve_contents_from_db ();
  if (result)
    _create_default_download_objects_from_db ();

g_dbus_connection_signal_subscribe (
pDnlMgrObj.system_dbus_connection, NULL, "net.connman.Manager", "PropertyChanged",
"/", NULL, G_DBUS_SIGNAL_FLAGS_NONE,
(GDBusSignalCallback) _newport_connman_property_changed_clb, NULL, NULL);

newport_check_for_preferred_network ();

  // create your own exit callback to take care of clearing and storing of resources
  dnl_mgr_register_exit_functionalities();
}

static gboolean dnl_mgr_termination_handler(gpointer data)
{
NEWPORT_DEBUG("Termination Handler");
  /* No need of error handling as this is the termination handler */
  store_all_running_downloads_in_db (NULL);
	g_list_free_full(pDnlMgrObj.pClientDownloadList,
			(GDestroyNotify) download_url_info_free);
	pDnlMgrObj.pClientDownloadList = NULL;
    exit(0);
	return G_SOURCE_REMOVE;
}

/*********************************************************************************************
 * Function:    dnl_mgrregister_exit_functionalities
 * Description: Register all functions that need to get executed for the
 *				different exit signals
 * Parameters:  None
 * Return:      None
 ********************************************************************************************/
static void dnl_mgr_register_exit_functionalities(void)
{
  g_unix_signal_add(SIGINT,dnl_mgr_termination_handler,NULL);
  g_unix_signal_add(SIGHUP,dnl_mgr_termination_handler,NULL);
  g_unix_signal_add(SIGTERM,dnl_mgr_termination_handler,NULL);

}

