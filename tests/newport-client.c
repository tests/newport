/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>
#include "org.apertis.Newport.Service.h"
#include "org.apertis.Newport.Download.h"
#include "newport-enums.h"
#include <libsoup/soup.h>

#define DOWNLOAD_FILE  "test_file.ogg"

#define APP_NAME "Download-Client"




static NewportService *proxy = NULL;

static void
test_newport_complete_download (gconstpointer user_data);

static gboolean
_default_event_source (gpointer user_data)
{
	  gboolean *timed_out = user_data;
	  *timed_out = TRUE;
	  return G_SOURCE_REMOVE;
}


static void
_download_error_changed (GObject *gobject, GParamSpec *pspec,
                         gpointer user_data)
{
  *(gboolean*) user_data = FALSE;
}

static void
check_for_pause_by_user_cb (GObject *gobject, GParamSpec *pspec,
                            gpointer user_data)
{
  guint state = newport_download_get_state (NEWPORT_DOWNLOAD (gobject));

  if (state == NEWPORT_DOWNLOAD_STATE_PAUSED_BY_USER)
    {
      *(gboolean *) user_data = TRUE;
    }
}

static void
check_download_success_cb (GObject *gobject, GParamSpec *pspec,
                           gpointer user_data)
{
  guint state = newport_download_get_state (NEWPORT_DOWNLOAD (gobject));

  if (state == NEWPORT_DOWNLOAD_STATE_SUCCESS)
    {
      *(gboolean *) user_data = TRUE;
    }
}

static void
check_download_started_cb (GObject *gobject, GParamSpec *pspec,
                           gpointer user_data)
{
  guint state = newport_download_get_state (NEWPORT_DOWNLOAD (gobject));

  if (state == NEWPORT_DOWNLOAD_STATE_IN_PROGRESS ||
      state == NEWPORT_DOWNLOAD_STATE_SUCCESS)
    {
      *(gboolean *) user_data = TRUE;
    }
}

static void
_download_info (NewportDownload *object, guint64 current_download_size,
                guint64 arg_total_file_size, guint64 arg_download_speed,
                guint64 arg_elapsed_time, guint64 arg_remain_time,
                gpointer user_data)
{
  *(gboolean *) user_data = TRUE;
}

static void
test_newport_complete_download (gconstpointer user_data)
{
  GError *error = NULL;
  gboolean state_result = FALSE;
  gchar *out_obj_path = NULL;
  NewportDownload *download_proxy;
  gboolean result;
  gchar *arg_download_path;
  gboolean timed_out = FALSE;
  guint timeout_id;
  const gchar *uri = user_data;

  arg_download_path =
    g_build_filename (g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
                      "newport-client", "musicfile", NULL);
  /* Call  download start to start downloading the file and wait till download-information signal quits the mainloop */
  result = newport_service_call_start_download_sync (proxy, uri,
                                                     arg_download_path,
                                                     &out_obj_path, NULL,
                                                     &error);
  g_free (arg_download_path);
  g_assert_no_error (error);
  g_assert (result);
  download_proxy =
    newport_download_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                             G_BUS_NAME_WATCHER_FLAGS_NONE,
                                             "org.apertis.Newport",
                                             out_obj_path, NULL, &error);
  g_free (out_obj_path);

  g_assert (newport_download_get_error (download_proxy) < 0);

  check_download_success_cb (G_OBJECT (download_proxy), NULL, &state_result);
  /*state_result flag set in download-information signal */
  g_signal_connect (download_proxy, "notify::error",
                    G_CALLBACK (_download_error_changed), &state_result);
  g_signal_connect (download_proxy, "notify::state",
                    G_CALLBACK (check_download_success_cb), &state_result);
  timeout_id = g_timeout_add_seconds (300, _default_event_source, &timed_out);
  while (!timed_out && !state_result)
    g_main_context_iteration (NULL, TRUE);
  g_assert (!timed_out);
  g_source_remove (timeout_id);
  g_assert (state_result);

}

static void
test_newport_get_download_list (void)
{
  gchar **out_paths = NULL;
  newport_service_call_get_downloads_sync (proxy, &out_paths, NULL, NULL);
  g_assert_nonnull (out_paths);
  g_strfreev (out_paths);

}

static void
test_newport_pause_download (gconstpointer user_data)
{
  GError *error = NULL;
  gboolean state_result = FALSE;
  gchar *out_obj_path = NULL;
  NewportDownload *download_proxy;
  gulong dwn_state_id;
  gboolean result;
  gboolean timed_out = FALSE;
  guint timeout_id;
  const gchar *uri = user_data;

  newport_service_call_get_download_for_url_sync (proxy, uri, &out_obj_path,
                                                  NULL, &error);
  download_proxy =
    newport_download_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                             G_BUS_NAME_WATCHER_FLAGS_NONE,
                                             "org.apertis.Newport",
                                             out_obj_path, NULL, NULL);
  g_free (out_obj_path);

  if (newport_download_get_state (download_proxy) != NEWPORT_DOWNLOAD_STATE_IN_PROGRESS)
    {
      g_test_skip ("Download is not in progress, So skip the test");
      g_object_unref (download_proxy);
      return;
    }

  check_for_pause_by_user_cb (G_OBJECT (download_proxy), NULL, &state_result);
  dwn_state_id = g_signal_connect (download_proxy, "notify::state",
                                   G_CALLBACK (check_for_pause_by_user_cb),
                                   &state_result);
  result = newport_download_call_pause_sync (download_proxy, NULL, &error);
  g_assert_no_error (error);
  g_assert (result);
  g_timeout_add_seconds (100, _default_event_source, &timed_out);
  timeout_id = g_timeout_add_seconds (100, _default_event_source, &timed_out);
  while (!timed_out && !state_result)
    g_main_context_iteration (NULL, TRUE);
  g_assert (!timed_out);
  g_source_remove (timeout_id);
  g_assert (state_result);
  g_signal_handler_disconnect (download_proxy, dwn_state_id);
}

static void
test_newport_cancel_download (gconstpointer user_data)
{
  GError *error = NULL;
  gchar *out_obj_path = NULL;
  gchar *out_obj_path2 = NULL;
  NewportDownload *download_proxy;
  gboolean result;
  const gchar *uri = user_data;

  newport_service_call_get_download_for_url_sync (proxy, uri, &out_obj_path,
                                                  NULL, NULL);
  download_proxy =
    newport_download_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                             G_BUS_NAME_WATCHER_FLAGS_NONE,
                                             "org.apertis.Newport",
                                             out_obj_path, NULL, NULL);
  g_free (out_obj_path);
  result = newport_download_call_cancel_sync (download_proxy, NULL, &error);
  g_assert_no_error (error);
  g_assert (result);
  newport_service_call_get_download_for_url_sync (proxy, uri,
                                                  &out_obj_path2, NULL, NULL);
  g_assert(out_obj_path2 == NULL);
}

static void
test_newport_resume_download (gconstpointer user_data)
{
  GError *error = NULL;
  gboolean state_result = FALSE;
  gchar *out_obj_path = NULL;
  NewportDownload *download_proxy;
  gulong dwn_state_id;
  gboolean result;
  gboolean timed_out = FALSE;
  guint timeout_id;
  const gchar *uri = user_data;

  newport_service_call_get_download_for_url_sync (proxy, uri, &out_obj_path,
                                                  NULL, &error);
  download_proxy =
    newport_download_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                             G_BUS_NAME_WATCHER_FLAGS_NONE,
                                             "org.apertis.Newport",
                                             out_obj_path, NULL, NULL);
  g_free (out_obj_path);

  check_download_started_cb (G_OBJECT (download_proxy), NULL, &state_result);
  dwn_state_id = g_signal_connect (download_proxy,
                                   "notify::state",
                                   G_CALLBACK (check_download_started_cb),
                                   &state_result);

  result = newport_download_call_resume_sync (download_proxy, NULL, &error);
  g_assert_no_error (error);
  g_assert (result);
  timeout_id = g_timeout_add_seconds (100, _default_event_source, &timed_out);
  while (!timed_out && !state_result)
    g_main_context_iteration (NULL, TRUE);
  g_assert (!timed_out);
  g_source_remove (timeout_id);
  g_assert (state_result);
  g_signal_handler_disconnect (download_proxy, dwn_state_id);
}

static void
test_newport_start_download (gconstpointer user_data)
{

  GError *error = NULL;
  gboolean state_result = FALSE;
  gchar *arg_download_path;
  gchar *out_obj_path = NULL;
  gboolean result;
  GError *error1 = NULL;
  NewportDownload *download_proxy;
  gboolean timed_out = FALSE;
  gulong dwn_state_id;
  gulong dwn_progress_id;
  guint timeout_id;
  const gchar *uri = user_data;

  arg_download_path =
    g_build_filename (g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
                      "newport-client", "zipfile", NULL);
  /* Call  download start to start downloading the file and wait till download-information signal quits the mainloop */
  result = newport_service_call_start_download_sync (proxy,
                                                     uri,
                                                     arg_download_path,
                                                     &out_obj_path,
                                                     NULL, &error);
  g_assert_no_error (error);
  g_assert (result);
  g_free (arg_download_path);

  download_proxy =
    newport_download_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                             G_BUS_NAME_WATCHER_FLAGS_NONE,
                                             "org.apertis.Newport",
                                             out_obj_path, NULL, &error1);
  g_free (out_obj_path);
  g_assert_no_error (error1);

  g_assert (newport_download_get_error (download_proxy) < 0);

  check_download_started_cb (G_OBJECT (download_proxy), NULL, &state_result);
  /*state_result flag set in download-information signal */
  g_signal_connect (download_proxy, "notify::error",
                    G_CALLBACK (_download_error_changed), &state_result);

  dwn_state_id = g_signal_connect (download_proxy, "notify::state",
                                   G_CALLBACK (check_download_started_cb),
                                   &state_result);

  dwn_progress_id = g_signal_connect (download_proxy, "download-progress",
                                      G_CALLBACK (_download_info),
                                      &state_result);

  timeout_id = g_timeout_add_seconds (100, _default_event_source, &timed_out);
  while (!timed_out && !state_result)
    g_main_context_iteration (NULL, TRUE);
  g_assert (!timed_out);
  g_source_remove (timeout_id);
  g_assert (state_result);
  g_signal_handler_disconnect (download_proxy, dwn_progress_id);
  g_signal_handler_disconnect (download_proxy, dwn_state_id);

}

static void
handle_request_cb (SoupServer *server, SoupMessage *msg, const char *path,
                   GHashTable *query, SoupClientContext *client,
                   gpointer user_data)
{
  g_autoptr (GByteArray) byte_array = NULL;
  guint i;

  byte_array = g_byte_array_new ();

  /* construct byte array with "text" */
  for (i = 0; i < 10000; i++)
    g_byte_array_append (byte_array, (const guint8 *) "text", 4);

  /* set the http status code to 200 which means request has been succeeded */
  soup_message_set_status (msg, 200);

  /* send the response to the clients with byte array content which is
   * of type "text/plain" */
  soup_message_set_response (msg, "text/plain", SOUP_MEMORY_COPY,
                             (const gchar *) byte_array->data, byte_array->len);
}

gint
main (gint argc, gchar *argv[])
{
  g_autoptr (SoupServer) server = NULL;
  g_autoptr (SoupURI) download_uri = NULL;
  GSList *uri_list = NULL;
  g_autofree gchar *download_uri_string = NULL;
  GError *error = NULL;
  int ret;

  g_test_init (&argc, &argv, NULL);

  server = soup_server_new (NULL, NULL);

  /* set up server to listen for connections on "localhost" */
  soup_server_listen_local (server, 0, SOUP_SERVER_LISTEN_IPV4_ONLY, &error);
  g_assert_no_error (error);

  /* Handle requests to download test-file */
  soup_server_add_handler (server, "/test-file", handle_request_cb, NULL, NULL);

  uri_list = soup_server_get_uris (server);

  /* Only one item in the list */
  g_assert_nonnull (uri_list->data);
  download_uri = soup_uri_new_with_base (uri_list->data, "/test-file");
  download_uri_string = soup_uri_to_string (download_uri, FALSE);

  g_slist_free_full (uri_list, (GDestroyNotify) soup_uri_free);

  proxy = newport_service_proxy_new_for_bus_sync (
      G_BUS_TYPE_SESSION, G_BUS_NAME_WATCHER_FLAGS_NONE, "org.apertis.Newport",
      "/org/apertis/Newport/Service", NULL, &error);
  g_test_add_data_func ("/newport-client/start-download", download_uri_string,
                        test_newport_start_download);
  g_test_add_data_func ("/newport-client/pause-download", download_uri_string,
                   test_newport_pause_download);
  g_test_add_data_func ("/newport-client/resume-download", download_uri_string,
                   test_newport_resume_download);
  g_test_add_data_func ("/newport-client/cancel-download", download_uri_string,
                   test_newport_cancel_download);
  g_test_add_data_func ("/newport-client/complete-download", download_uri_string,
                   test_newport_complete_download);
  g_test_add_func ("/newport-client/get-download-list",
                   test_newport_get_download_list);
  ret = g_test_run ();

  return ret;
}

