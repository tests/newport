/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __NEWPORT_ERRORS__
#define __NEWPORT_ERRORS__

#include <glib.h>

/**
 * NewportDownloadManagerError:
 * @NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED: Generic error.
 * @NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK: Network error.
 * @NEWPORT_DOWNLOAD_MANAGER_ERROR_NO_MEMORY: No memory.
 * @NEWPORT_DOWNLOAD_MANAGER_ERROR_UNRECOGNIZED_FORMAT: Unrecognized format.
 *
 * Download manager error.
 */
typedef enum
{
    NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED = 0,
    NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK,
    NEWPORT_DOWNLOAD_MANAGER_ERROR_NO_MEMORY,
    NEWPORT_DOWNLOAD_MANAGER_ERROR_UNRECOGNIZED_FORMAT,
    /*< private >*/
    NEWPORT_N_DOWNLOAD_MANAGER_ERRORS /*< skip >*/
} NewportDownloadManagerError;

#define NEWPORT_DOWNLOAD_MANAGER_ERROR newport_download_manager_error_quark()
GQuark newport_download_manager_error_quark (void);

#endif
