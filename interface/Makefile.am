# Makefile to build all of the service code in the fi/ subdirectories. This uses
# non-recursive automake to allow the services to be built in parallel, and
# allow sharing of build rules in this file.

# To add a new service:
#  1. Add a section for it to this file, appending to lib_LTLIBRARIES and
#     includesfi_HEADERS.
#  2. Add it to the library list in docs/reference/Makefile.am.
#  3. Add its documentation to the includes in
#     docs/reference/canterbury-docs.xml.
#  4. Add it to the library list and CPPFLAGS in sources/Makefile.am.
# Note that as this is all generated code, we disable various warnings.

service_cppflags = \
        $(AM_CPPFLAGS)
service_cflags = \
        -Wno-error \
        -Wno-strict-aliasing \
        -Wno-redundant-decls \
        $(GLIB_CFLAGS) \
        $(CODE_COVERAGE_CFLAGS) \
        $(AM_CFLAGS)
service_libadd = \
        $(GLIB_LIBS) \
        $(CODE_COVERAGE_LIBS) \
        $(AM_LIBADD)
service_ldflags = \
        $(ERROR_LDFLAGS) \
        $(AM_LDFLAGS)
service_codegen_flags = \
        --generate-docbook docs \
        --c-namespace=Newport \
	--c-generate-autocleanup=all \
        --interface-prefix=org.apertis.Newport \
        $(NULL)

# Generic rules
%.c %.h docs-%.xml: %.xml
	        $(AM_V_GEN)$(GDBUS_CODEGEN) \
                $(service_codegen_flags) --generate-c-code $* $<

newport_sources_h = $(newport_sources_xml:.xml=.h)
newport_sources_c = $(newport_sources_xml:.xml=.c)
newport_built_docs = $(addprefix docs-,$(newport_sources_xml))

newport-enumtypes.h: enumtypes.h.template $(includesfi_HEADERS)
	$(AM_V_GEN)$(GLIB_MKENUMS) --template $(filter %.template,$^) \
		$(filter-out %.template,$^) \
		> $@.tmp && mv $@.tmp $@

newport-enumtypes.c: enumtypes.c.template $(includesfi_HEADERS)
	$(AM_V_GEN)$(GLIB_MKENUMS) --template $(filter %.template,$^) \
		$(filter-out %.template,$^) \
		> $@.tmp && mv $@.tmp $@

BUILT_SOURCES = \
	$(newport_sources_h) \
	$(newport_sources_c) \
	$(newport_built_docs) \
	newport-enumtypes.h \
	newport-enumtypes.c \
	$(NULL)

EXTRA_DIST = \
	$(newport_sources_xml) \
	enumtypes.c.template \
	enumtypes.h.template \
	$(NULL)

CLEANFILES = \
	$(BUILT_SOURCES) \
	$(NULL)

includesfidir = $(includedir)/newport

nodist_includesfi_HEADERS = \
	$(newport_sources_h) \
	newport-enumtypes.h \
	$(NULL)

includesfi_HEADERS = \
	newport-enums.h \
	newport-errors.h \
	$(NULL)

# newport-app-db-handler
newport_sources_xml = org.apertis.Newport.Service.xml org.apertis.Newport.Download.xml

#newport_sources_xml += $(newport_recorder_sources_xml)

lib_LTLIBRARIES = libnewportiface.la

libnewportiface_la_SOURCES = \
	$(includesfi_HEADERS) \
	newport-errors.c \
	$(NULL)

nodist_libnewportiface_la_SOURCES = \
    newport-enumtypes.h \
    newport-enumtypes.c \
    $(newport_sources_xml:.xml=.c) \
    $(newport_sources_xml:.xml=.h) \
    $(NULL)

libnewportiface_la_CPPFLAGS = $(service_cppflags) \
			      -DG_LOG_DOMAIN=\"Newport\"
libnewportiface_la_CFLAGS = $(service_cflags)
libnewportiface_la_LIBADD = $(service_libadd)
libnewportiface_la_LDFLAGS = $(service_ldflags)

-include $(INTROSPECTION_MAKEFILE)
INTROSPECTION_GIRS =
INTROSPECTION_SCANNER_ARGS = \
	$(WARN_SCANNERFLAGS) \
	$(NULL)

if HAVE_INTROSPECTION

NewportInterface-@NEWPORT_API_VERSION@.gir: libnewportiface.la
NewportInterface_@NEWPORT_API_VERSION@_gir_FILES = \
	$(libnewportiface_la_SOURCES) \
	$(nodist_libnewportiface_la_SOURCES) \
	$(NULL)
NewportInterface_@NEWPORT_API_VERSION@_gir_NAMESPACE = NewportInterface
NewportInterface_@NEWPORT_API_VERSION@_gir_VERSION = @NEWPORT_API_VERSION@
NewportInterface_@NEWPORT_API_VERSION@_gir_LIBS = libnewportiface.la
NewportInterface_@NEWPORT_API_VERSION@_gir_CFLAGS = $(AM_CPPFLAGS) $(CPPFLAGS)
NewportInterface_@NEWPORT_API_VERSION@_gir_SCANNERFLAGS = \
	--identifier-prefix=Newport \
	--symbol-prefix=newport \
	$(WARN_SCANNERFLAGS) \
	$(NULL)
NewportInterface_@NEWPORT_API_VERSION@_gir_INCLUDES = \
	GLib-2.0 \
	Gio-2.0 \
	$(NULL)

INTROSPECTION_GIRS += NewportInterface-@NEWPORT_API_VERSION@.gir

girdir = $(datadir)/gir-1.0
gir_DATA = $(INTROSPECTION_GIRS)

typelibdir = $(libdir)/girepository-1.0/
typelib_DATA = $(INTROSPECTION_GIRS:.gir=.typelib)

CLEANFILES += $(gir_DATA) $(typelib_DATA)
endif

-include $(top_srcdir)/git.mk
